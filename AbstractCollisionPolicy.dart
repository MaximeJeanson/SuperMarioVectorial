library CollicionPolicy;

abstract class AbstractCollisionPolicy {
  void collide();

  bool canCollide();
}

class NoCollisionPolicy extends AbstractCollisionPolicy {
  void collide() {}

  @override
  bool canCollide() {
    return false;
  }
}

class WeakCollisionPolicy extends AbstractCollisionPolicy {
  void collide() {}

  @override
  bool canCollide() {
    return true;
  }
}

class StrongCollisionPolicy extends AbstractCollisionPolicy {
  void collide() {}

  @override
  bool canCollide() {
    return true;
  }
}
