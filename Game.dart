import 'dart:async';
import 'dart:html';
import 'dart:math' as Math;
import 'dart:svg';

import 'AbstractCollisionPolicy.dart';

List<String> _map2 = [
  "                                          ",
  "                                          ",
  "                                          ",
  "                                          ",
  "                                          ",
  "                                          ",
  "                                          ",
  "                                          ",
  "            22222                 1       ",
  "   2                             11       ",
  "   2                            111       ",
  "1111111  1111111111111111   11111111111111",
  "1111111  1111111111111111   11111111111111",
  "1111111                     11111111111111",
  "1111111111111111111111111   11111111111111",
];

List<String> _map = [
  "                                                                           ",
  "                                                                           ",
  "                                                                           ",
  "                                                                           ",
  "                               lll                                      lll",
  "                          ?                                                ",
  "                                                                           ",
  "                                                                           ",
  "         tt                                                                ",
  "         tt         ?   s?s?s                  tt         tt     ?         ",
  "         tt                              tt    tt         tt               ",
  "         tt                    tt        tt    tt         tt               ",
  "         tt      bbb m   k  b  tt        ttkbb tt     kk  tt bbb m     b   ",
  "cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc  ccccc",
  "cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc  ccccc",
];

Map<String, String> textures = {
  " ": "assets2.svg#air",
  "s": "assets2.svg#stone",
  "c": "assets2.svg#cobblestone",
  "b": "assets2.svg#bush",
  "m": "assets2.svg#mountain",
  "k": "assets2.svg#kupa",
  "w": "assets2.svg#waste",
  "t": "assets2.svg#pipe",
  "l": "assets2.svg#cloud",
  "?": "assets2.svg#gift",
};

Map<String, AbstractCollisionPolicy> collisionPolicies = {
  " ": new NoCollisionPolicy(),
  "s": new StrongCollisionPolicy(),
  "c": new StrongCollisionPolicy(),
  "b": new NoCollisionPolicy(),
  "m": new NoCollisionPolicy(),
  "k": new NoCollisionPolicy(),
  "w": new WeakCollisionPolicy(),
  "t": new StrongCollisionPolicy(),
  "l": new NoCollisionPolicy(),
  "?": new WeakCollisionPolicy(),
};

class Pos {
  double x = 0.0;
  double y = 0.0;

  Pos(this.x, this.y) {}
}

abstract class Entity {
  Pos position;

  void render();

  void tick();

  void jump();

  Entity(this.position) {}
}

enum PlayerState { Flower, Young, Adult }

class Vector2D {
  double x, y;

  double scalar() {
    return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
  }

  Vector2D normalize() {
    double d = scalar();

    return new Vector2D(x / d, y / d);
  }

  Vector2D(this.x, this.y) {}

  static Vector2D crossProduct(Vector2D first, Vector2D last) {
    return new Vector2D(first.x * last.y, -first.y * last.x);
  }

  Vector2D operator *(double amplitude) {
    x *= amplitude;
    y *= amplitude;

    return this;
  }
}

enum Side { Top, Bottom, Left, Right }

class Player extends Entity {
  static Map<PlayerState, double> PLAYER_STATE_SIZES = {
    PlayerState.Adult: 2.0,
    PlayerState.Young: 1.0,
    PlayerState.Flower: 2.0,
  };
  static const double jumpFactor = 200.0;
  static const int jumpStun = 5;
  static const double GRAVITY = 0.0008970;
  bool hasJump = false;

  Vector2D velocity = new Vector2D(0.0, 0.0);
  Vector2D acceleration = new Vector2D(5.0, -GRAVITY);

  double t0;
  Vector2D _velocity;
  Vector2D _position;


  UseElement element;
  Stage stage;

  PlayerState state = PlayerState.Young;

  int grow = 20;
  bool jumping = false;
  bool walking = false;

  Timer walkTimer = null;

  @override
  void render() {
    element.setAttribute("x", (position.x * 16.0).toString() + "px");
    element.setAttribute("y", (position.y * 16.0).toString() + "px");
  }

  @override
  void tick() {
//    print("Tick Player");

    if (grow == 0) {
      element.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href",
          "assets2.svg#marioAdult");
      state = PlayerState.Adult;
    }
  }

//  double t0;

  Side collisionSide(Block block, double x, double y,
      [double collisionPrecision = 0.05]) {
    double _collisionPrecision = 16.0;
    double size = -2 * collisionPrecision + 32.0;

    Map<Side, Math.Rectangle<double>> outsets = {
      Side.Top: new Math.Rectangle<double>((x + collisionPrecision) * 32.0,
          (y + 1) * 32.0, size, _collisionPrecision),
      Side.Bottom: new Math.Rectangle<double>((x + collisionPrecision) * 32.0,
          y * 32.0 - _collisionPrecision, size, _collisionPrecision),
      Side.Left: new Math.Rectangle<double>(x * 32.0 - _collisionPrecision,
          (y + collisionPrecision) * 32.0, _collisionPrecision, size),
      Side.Right: new Math.Rectangle<double>((x + 1) * 32.0,
          (y + collisionPrecision) * 32.0, _collisionPrecision, size),
    };

    Side biggestSide = null;
    double biggestValue = 0.0;

    outsets.forEach((Side key, Math.Rectangle<double> value) {
      Math.Rectangle<double> intersect = value.intersection(block.boundingBox);
      double area = 0.0;

      if (intersect != null) area = intersect.width * intersect.height;

      if (area > biggestValue) {
        biggestSide = key;
        biggestValue = area;
      }
    });

    if (biggestSide == Side.Top) print("BUG");

    return biggestSide;
  }

  dynamic collisionCheck(double x, double y,
      [double collisionPrecision = 0.05]) {
//    const double collisionPrecision = 0.1;

    Math.Rectangle<double> outset = new Math.Rectangle<double>(
        (x * 32.0) + collisionPrecision,
        (y * 32.0) + collisionPrecision,
        32.0 - (2 * collisionPrecision),
        PLAYER_STATE_SIZES[state] * 32.0 - (2 * collisionPrecision));

    int xCeil = x.ceil() + 1;
    int xFloor = x.floor() - 1;
    int yCeil = (y + PLAYER_STATE_SIZES[state]).ceil() + 1;
    int yFloor = y.floor();

    Side side = null;
    List<Side> _side = [];
    bool collide = false;

    for (Block block in stage.blocks) {
      if (xCeil < block.position.x || xFloor > block.position.x) continue;
      if (yCeil < block.position.y || yFloor > block.position.y) continue;

      if (block.canCollide() &&
          outset.intersection(block.boundingBox) != null) {
        double dx = block.position.x - x;
        double dy = block.position.y - y;

        double __dx = dx.abs();
        double __dy = dy.abs();

        double _dx = Math.min(1 - __dx, __dx);
        double _dy = Math.min(1 - __dy, __dy);

        side = (_dx < _dy
            ? (dx < 0.0 ? Side.Left : Side.Right)
            : (dy < 0.0 ? Side.Bottom : Side.Top));

        side = collisionSide(block, x, y, collisionPrecision * 10);
        _side.add(side);

        switch (side) {
          case Side.Top:
            y = block.position.y - 1.0;
            break;
          case Side.Bottom:
            y = block.position.y + PLAYER_STATE_SIZES[state];
            break;
          case Side.Left:
            x = block.position.x + 1.0;
            break;
          case Side.Right:
            x = block.position.x - 1.0;
            break;
        }

        collide = true;
      }
    }

    return [x, y, collide, _side];
  }

  @override
  void jump() {
//    const int halfLife = 15;
//
//    if (jumping) return;
//
//    jumping = true;
//    double jmp = -jumpFactor;
//    double startY = position.y.toDouble();
//    acceleration.y += 0.5;
//
//    int i = 0;
//    new Timer.periodic(new Duration(milliseconds: 30), (Timer timer) {
//      if (++i == halfLife) acceleration.y -= 0.5;
//
//      dynamic c = collisionCheck(position.x * 32.0, position.y * 32.0, -0.5);
//
//      if (c[2] && (c[3] as List<Side>).contains(Side.Bottom)) {
//        new Timer(new Duration(milliseconds: jumpStun), () {
//          jumping = false;
//        });
//
//        timer.cancel();
//      }
//    });
//    new Timer.periodic(new Duration(microseconds: 10), (Timer timer) {
//      double j = jmp / jumpFactor;
//
//      double y = (-acceleration.y * (j * j)) + (startY + acceleration.y);
//
//      if (y < 0) {
//        print("DEAD");
//        y = 0.0;
//        position.y = 0.0;
//        timer.cancel();
//        new Timer(new Duration(milliseconds: jumpStun), () {
//          jumping = false;
//        });
//      }
//
//      var colliding = collisionCheck(position.x, y);
//
////      y = colliding[1];
//
//      if (colliding[2]) {
//        timer.cancel();
//        position.y = colliding[1];
//
//        new Timer(new Duration(milliseconds: jumpStun), () {
//          jumping = false;
//        });
//      }
//
//      position.y = y;
//      jmp++;
//    });
  }

  void walk(Vector2D direction) {
    if (jumping && direction.y == 1.0) return;

    const double accelerationCap = 0.05;

    walking = !(walking && direction.x == 0);
//    jumping = !(jumping && direction.y == 0);

    acceleration.x = accelerationCap * direction.x;
//    acceleration.y += 2 * GRAVITY * direction.y;

    velocity.y = 20 * direction.y;

    if (direction.y == 1.0) {
      jumping = true;

      print(jumping);

      new Timer(new Duration(milliseconds: 40), () {
//        if (jumping) acceleration.y -= 2 * GRAVITY;
        hasJump = true;
      });
      new Timer(new Duration(milliseconds: 20), () {
        if (jumping) hasJump = true;
      });
    }

    if(direction.scalar() != 0) {
      t0 = new DateTime.now().second.toDouble();
      _velocity = velocity;
      _position = new Vector2D(position.x, position.y);
    }

    if (walkTimer != null) return;


    walkTimer =
        new Timer.periodic(new Duration(milliseconds: 1), (Timer timer) {
//      double t = 0.04;
      double t = (new DateTime.now().second.toDouble() - t0).abs();

      double vx = _velocity.x * t + acceleration.x * t;
      double vy = _velocity.y * t + acceleration.y * t;

      double x = _position.x + (_velocity.x + vx) * t / 2;
      double y = _position.y + (_velocity.y + vy) * t / 2;

      if (x < 0.0) x = 0.0;
      if (y < 0.0) y = 0.0;

      dynamic c = collisionCheck(x, y);

      if (jumping &&
          hasJump &&
          c[2] &&
          (c[3] as List<Side>).contains(Side.Bottom)) {
        new Timer(new Duration(milliseconds: jumpStun), () {
          jumping = false;
          hasJump = false;
        });
      }

      double _x = c[0];
      double _y = c[1];

      if ((_x - x).abs() > 0.5) print(
          "Error: Collision value error - You got teleported from " +
              position.x.toString() +
              " - " +
              x.toString() +
              " to " +
              _x.toString());

      if ((_y - y).abs() > 0.5) print(
          "Error: Collision value error - You got teleported from " +
              position.y.toString() +
              " - " +
              y.toString() +
              " to " +
              _y.toString());

      if (c[3] == Side.Top || c[3] == Side.Bottom) vy = 0.0;
      if (c[3] == Side.Left || c[3] == Side.Right) vx = 0.0;

      if (_x - stage.xv > 8) stage.xv = _x - 7.5;
      if (_x - stage.xv < 8 && stage.xv > 0.0) stage.xv = _x - 8;

      position.x = _x;
      position.y = _y;

      velocity.x = vx;
      velocity.y = vy;

//          t0 = new DateTime.now().second.toDouble();
    });
  }

  Player(Pos position, this.stage) : super(position) {
    element = new SvgElement.tag("use");
    element.setAttributeNS(
        "http://www.w3.org/1999/xlink", "xlink:href", "assets2.svg#mario");
    element.setAttribute("x", (position.x * 16).toString() + "px");
    element.setAttribute("y", (position.y * 16).toString() + "px");
  }
}

class Block<CollisionPolicy> {
  String texturePath;
  DocumentFragment root;
  UseElement element;
  Math.Point<int> position;

  Math.Rectangle<double> boundingBox;

  AbstractCollisionPolicy collisionPolicy;

  void render() {}

  void tick() {}

  void collide() {
    collisionPolicy.collide();
  }

  bool canCollide() {
    return collisionPolicy.canCollide();
  }

  Block(this.collisionPolicy, this.position, this.texturePath) {
    element = new SvgElement.tag("use");
    element.setAttributeNS(
        "http://www.w3.org/1999/xlink", "xlink:href", texturePath);
    element.setAttribute("x", (position.x * 16).toString() + "px");
    element.setAttribute("y", (position.y * 16).toString() + "px");

    boundingBox = new Math.Rectangle<double>(
        position.x * 32.0, position.y * 32.0, 32.0, 32.0);
  }
}

class Stage {
  List<Block> blocks = new List();
  GElement element;

  double xv = 0.0;

  Stage() {
    int y = _map.length - 1;

    _map.forEach((String line) {
      for (int x = 0; x < line.length; ++x) {
        String token = line[x];
        if (token != " ") blocks.add(new Block(
            collisionPolicies[token], new Math.Point(x, y), textures[token]));
      }
      --y;
    });

    element = new GElement();
    element.setAttribute("transform", "matrix(1, 0, 0, -1, 0, 194)");

    blocks.add(new Block(
        new NoCollisionPolicy(), new Math.Point<int>(0, 0), textures["t"]));

    blocks.forEach((Block block) {
      element.append(block.element);
    });
  }

  void render() {
    blocks.forEach((Block block) {
      block.render();
    });
  }

  void tick() {
    blocks.forEach((Block block) {
      block.tick();
    });
  }
}

enum GameState { Started, Ended, Paused }

class Game {
  static const int FPS = 30;
  static const int TPS = 3;
  static List<Vector2D> UNITS = [
    new Vector2D(1.0, 0.0), // Right
    new Vector2D(-1.0, 0.0), // Left
    new Vector2D(0.0, 1.0), // Top
    new Vector2D(0.0, -1.0), // Bottom
    new Vector2D(0.0, 0.0), // Null
  ];

  Vector2D direction = UNITS[4];

  List<Entity> entities = new List();
  GameState state = GameState.Started;
  Stage stage = new Stage();
  Timer tickTimer;
  Timer renderTimer;
  SvgSvgElement viewport;

  Element fpsLabel;
  Element tpsLabel;
  int fps = 0;
  int tps = 0;

  Player player;

  Map<int, bool> keys = {
    KeyCode.D: false,
    KeyCode.A: false,
    KeyCode.W: false,
    KeyCode.S: false
  };

  void tick() {
    ++tps;
    if (state != GameState.Started) return;
//    print("Tick Game");
    stage.tick();

    player.tick();

    entities.forEach((Entity entity) {
      entity.tick();
    });
  }

  void render() {
    viewport.setAttribute(
        "viewBox", (stage.xv * 16.0).toString() + " 0 240 194");

    ++fps;
    if (state != GameState.Started) return;
//    print("Render Game");
    stage.render();
    player.render();
    entities.forEach((Entity entity) {
      entity.render();
    });
  }

  Game() {
    player = new Player(new Pos(3.0, 5.0), stage);

    fpsLabel = new SpanElement();
    fpsLabel.classes.add("FPSLabel");
    tpsLabel = new SpanElement();
    tpsLabel.classes.add("TPSLabel");

    viewport = new SvgSvgElement();
    viewport.setAttribute("viewBox", "0 0 240 194");
    viewport.setAttribute("width", "480px");
    viewport.setAttribute("height", "388px");

    viewport.setAttribute("preserveAspectRatio", "xMidYMid slice");
    viewport.append(stage.element);
    stage.element.append(player.element);
    document.body.append(viewport);
    document.body.append(fpsLabel);
    document.body.append(tpsLabel);

    new Timer.periodic(new Duration(seconds: 1), (Timer timer) {
      int _fps = fps;
      int _tps = tps;
      fps = 0;
      tps = 0;

      fpsLabel.text = _fps.toString() + " FPS";
      tpsLabel.text = _tps.toString() + " TPS";
    });
  }

  void start() {
    tickTimer = new Timer.periodic(new Duration(milliseconds: 1000 ~/ TPS),
        (Timer timer) {
      tick();
    });

    renderTimer = new Timer.periodic(new Duration(milliseconds: 1000 ~/ FPS),
        (Timer timer) {
      render();
    });

    window.onKeyDown.listen((KeyboardEvent event) {
//      print(event.type);
      switch (event.keyCode) {
        case KeyCode.D:
          if (!keys[event.keyCode]) {
            direction.x = 1.0;

            player.walk(direction);

            keys[event.keyCode] = true;
          }
          break;
        case KeyCode.A:
          if (!keys[event.keyCode]) {
            direction.x = -1.0;

            player.walk(direction);

            keys[event.keyCode] = true;
          }
          break;
        case KeyCode.W:
        case KeyCode.SPACE:
          if (!keys[KeyCode.W]) {
            direction.y = 1.0;

            player.walk(direction);
//          player.position.y++;
            keys[KeyCode.W] = true;
          }
          break;
        case KeyCode.S:
          player.position.y--;
          break;
      }
    });
    window.onKeyUp.listen((KeyboardEvent event) {
      print(event.type);

      switch (event.keyCode) {
        case KeyCode.P:
        case KeyCode.PAUSE:
        case KeyCode.ESC:
          state =
              state == GameState.Paused ? GameState.Started : GameState.Paused;

          break;

        case KeyCode.D:
        case KeyCode.A:
          keys[event.keyCode] = false;
          direction.x = 0.0;

          player.walk(direction);
          break;
        case KeyCode.W:
        case KeyCode.SPACE:
          keys[KeyCode.W] = false;

          direction.y = 0.0;

//          player.jump();
//          player.position.y++;
          break;
        case KeyCode.S:
          player.position.y--;
          break;
      }
    });
  }
}

int main() {
  Game g = new Game();
  g.start();

  return 0;
}
